#include "b_tree.h"
#include "shared_lib.h"

struct node_type
{
    Item data;
    struct node_type *left;
    struct node_type *right;
};

static void terminate(const char *message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}
static Binary_tree minValueNode(Binary_tree node)
{
    Binary_tree current = node;

    while (current != NULL && current->left != NULL)
        current = current->left;

    return current;
}

static void binary_tree_print_2D_Util(Binary_tree root, int space)
{
    int count = 10;

    if (root == 0)
        return;
    space += count;

    binary_tree_print_2D_Util(root->right, space);
    printf("\n");
    for (int i = count; i < space; i++)
        printf(" ");
    printf("%d\n", root->data);

    binary_tree_print_2D_Util(root->left, space);
}

static Binary_tree binary_tree_create_node(Item data)
{
    Binary_tree node = malloc(sizeof(Binary_tree));
    if (node == NULL)
        terminate("Error in create a binary tree!");
    node->left = NULL;
    node->right = NULL;
    node->data = data;

    return node;
}

Binary_tree binary_tree_insert_node(Binary_tree node, Item data)
{
    // ignoring a repeated item

    if (node == NULL)
        return binary_tree_create_node(data);

    // recursion
    if (node->data > data)
        node->left = binary_tree_insert_node(node->left, data);
    else if (node->data < data)
        node->right = binary_tree_insert_node(node->right, data);

    return node;
}

Binary_tree binary_tree_delete_node(Binary_tree root, Item data)
{
    if (root == NULL)
        return root;

    if (root->data > data)
        root->left = binary_tree_delete_node(root->left, data);
    else if (root->data < data)
        root->right = binary_tree_delete_node(root->right, data);
    else // a node was founded
    {
        // one child or no child
        if (root->left == NULL)
        {
            Binary_tree temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL)
        {
            Binary_tree temp = root->left;
            free(root);
            return temp;
        }

        // both children
        // get smallest in the right subtree
        Binary_tree temp = minValueNode(root->right);

        // copy sucessors's content
        root->data = temp->data;

        // delete sucessor
        root->right = binary_tree_delete_node(root->right, temp->data);
    }
    return root;
}
void binary_tree_print_2D(Binary_tree root)
{

    binary_tree_print_2D_Util(root, 0);
}

void binary_tree_print_pre_order(Binary_tree root)
{
    if (root == NULL)
        return;

    printf("%d ", root->data);

    binary_tree_print_pre_order(root->left);
    binary_tree_print_pre_order(root->right);
}

void binary_tree_print_post_order(Binary_tree root)
{
    if (root == NULL)
        return;
    
    binary_tree_print_pre_order(root->left);
    binary_tree_print_pre_order(root->right);

    printf("%d ", root->data);
}

Binary_tree binary_tree_destroy(Binary_tree root)
{
    if(root != NULL)
    {
        binary_tree_destroy(root->left);
        binary_tree_destroy(root->right);
        free(root);
    }
    return NULL;
}

