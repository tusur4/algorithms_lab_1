#ifndef B_TREE_H
#define B_TREE_H

#include "shared_lib.h"

typedef struct node_type *Binary_tree;

Binary_tree binary_tree_insert_node(Binary_tree node, Item data);
Binary_tree binary_tree_delete_node(Binary_tree root, Item data);
void binary_tree_print_2D(Binary_tree root);
void binary_tree_print_pre_order(Binary_tree root);
void binary_tree_print_post_order(Binary_tree root);
Binary_tree binary_tree_destroy(Binary_tree root);

#endif