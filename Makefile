# target: dependences
# 	action

client: client.o b_tree.o array_list.o tests.o
	gcc -o client client.o b_tree.o array_list.o tests.o
client.o : client.c  array_list.h array_list.h shared_lib.h tests.h
	gcc -c client.c
b_tree.o: b_tree.c b_tree.h shared_lib.h
	gcc -c b_tree.c
array_list.o: array_list.c array_list.h shared_lib.h
	gcc -c array_list.c
tests.o: tests.c tests.h
	gcc -c tests.c
clean:
	del /Q /F client.exe *.o
