#include "array_list.h"
#include "b_tree.h"
#include "tests.h"

void test_array(void)
{
    printf("BEGIN\n");
    printf("TEST: Array_list\n");

    Array_list a;
    a = array_list_create(2);
    array_list_add(1, a);
    printf("add: %d\n", array_list_get(0, a));
    printf("length: %d\n", array_list_length(a));
    array_list_add(2, a);
    printf("add: %d\n", array_list_get(1, a));
    array_list_add(3, a);
    printf("add: %d\n", array_list_get(2, a));
    printf("length: %d\n", array_list_length(a));
    printf("amount of elements: %d\n", array_list_amount_elements(a));

    array_list_make_empty(a);
    printf("make_empty\n");
    printf("length: %d\n", array_list_length(a));
    printf("amount of elements: %d\n", array_list_amount_elements(a));

    array_list_add(4, a);
    array_list_add(5, a);
    array_list_add(6, a);
    printf("add: %d\n", array_list_get(0, a));
    printf("add: %d\n", array_list_get(1, a));
    printf("add: %d\n", array_list_get(2, a));
    printf("length: %d\n", array_list_length(a));

    array_list_destroy(a);
    printf("destroy\n");
    printf("length: %d\n", array_list_length(a));

    printf("END\n");
}

void test_binary_tree(void)
{
    printf("BEGIN\n");
    printf("TEST: binary_tree\n");

    Binary_tree root = NULL;
    root = binary_tree_insert_node(root, 50);
    root = binary_tree_insert_node(root, 30);
    root = binary_tree_insert_node(root, 20);
    root = binary_tree_insert_node(root, 40);
    root = binary_tree_insert_node(root, 70);
    root = binary_tree_insert_node(root, 60);
    root = binary_tree_insert_node(root, 80);
 
    binary_tree_print_2D(root);

    printf("\nDelete 70\n");
    binary_tree_delete_node(root, 70);
    printf("After delete");
    binary_tree_print_2D(root);
    binary_tree_print_pre_order(root);
    printf("\n");
    binary_tree_print_post_order(root);
    printf("\n");
    binary_tree_destroy(root);
    printf("binary_tree_destroy\n");
    printf("END\n");
}