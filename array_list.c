#include "array_list.h"
#include "shared_lib.h"

//Struct Hack, content[] - for C99 or content[1] for C89 
struct array_type
{
    int len;
    int amount_elements;
    Item content[];
};

static void terminate(const char *message)
{
    printf("%s\n", message);
    exit(EXIT_FAILURE);
}

Array_list array_list_create(int initial_size)
{
    unsigned int length = sizeof(Item) * initial_size;
    Array_list a = malloc(sizeof(a) + length);
    if(a == NULL)
        terminate("Error in create Array_list!");
    a->amount_elements = 0;
    a->len = initial_size;
}

void array_list_add(Item data, Array_list a)
{
    int new_len;
    if (a->amount_elements >= a->len - 1)
    {
        a->len += 10;
        a = realloc(a, sizeof(struct array_type) + (sizeof(Item)) * a->len);
    }
    if(a == NULL)
        terminate("Error re-allocate memory!");
    a->content[a->amount_elements ++] = data;
}
Item array_list_get(int index, Array_list a)
{
    return a->content[index];
}

int array_list_length(Array_list a)
{
    return a->len;
}

int array_list_amount_elements(Array_list a)
{
    return a->amount_elements;
}

void array_list_make_empty(Array_list a)
{
    array_list_destroy(a);
    a = array_list_create(0);
}
void array_list_destroy(Array_list a)
{
    free(a);
}