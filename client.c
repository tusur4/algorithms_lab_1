#include "array_list.h"
#include "b_tree.h"
#include "tests.h"

Array_list build_array(void);
Binary_tree build_tree(Array_list a);

int main(void)
{
    //***uncomment for tests
    //test_array();
    //test_binary_tree();

    Array_list seq_1, seq_2;
    Binary_tree root_1, root_2;

    printf("Enter two sequences of numbers. * - means STOP insertion.\n");
    for(int i = 1; i < 3; i++)
    {
        printf("%d sequences of numbers:\n", i);
        if(i == 1)
            seq_1 = build_array();
        else
            seq_2 = build_array();
    }

    if((array_list_amount_elements(seq_1) && array_list_amount_elements(seq_2)) == 0)
        {
            printf("Error of input!");
            return 0;
        }

    //building a binary tree

    root_1 = build_tree(seq_1);
    root_2 = build_tree(seq_2);

    //destroying sequences
    array_list_destroy(seq_1);
    array_list_destroy(seq_2);
    
    //printing a binary tree
    binary_tree_print_2D(root_1);
    binary_tree_print_pre_order(root_1);
    binary_tree_print_2D(root_2);
    binary_tree_print_pre_order(root_2);
    
    //destroying a binary tree
    binary_tree_destroy(root_1);
    binary_tree_destroy(root_2);

    return 0;
}

Array_list build_array(void)
{
    Item element;
    char ch;
    Array_list a = array_list_create(5);
    for(;;)
    {
        scanf("%d",&element);
        ch = getchar();
        if(ch == '*')
            return a;
        array_list_add(element, a);
    }
    return a;
}

Binary_tree build_tree(Array_list a)
{
    Binary_tree t = NULL;

    for(int i = 0; i < array_list_amount_elements(a); i++)
        if (!i)
            t = binary_tree_insert_node(t, array_list_get(i, a));
        else
            binary_tree_insert_node(t, array_list_get(i, a));
    return t;
}